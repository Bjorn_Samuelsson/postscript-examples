# Minimalist PostScript

This directory contains small PostScript programs with 2 to 4 lines
(less than 80 columns wide) between `%!PS` and `showpage`. The output
is suitable for poirtrait A4 or letter formats although there is no
metadata for boundingbox or page size in the source files.

[Sierpinski gasket](sierpinski.ps)

[Spiral with the golden angle](spiral.ps)

[Penrose tiling](penrose.ps)

